import os
import urllib
import jinja2
import webapp2
from google.appengine.api import users
from models import League, Club, Player, Admin

JINJA_EN = jinja2.Environment(
	loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
	extensions=['jinja2.ext.autoescape'])

class MainPage(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/index.html')
		self.response.write(template.render({}))

class login(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/login.html')
		self.response.write(template.render({}))

class register(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/register.html')
		self.response.write(template.render({}))

class playerList(webapp2.RequestHandler):
	def get(self):

		players_query = Player.query().order(Player.name)
		players = players_query.fetch(10)

		template_values = {
		'players': players
		}

		template = JINJA_EN.get_template('templates/playerlist.html')
		self.response.write(template.render({template_values}))

class privacyPolicy(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/privacypolicy.html')
		self.response.write(template.render({}))

class adminPage(webapp2.RequestHandler):
	def get(self):

		# Get the current user. If user is not logged in, asks them to login.
		user = users.get_current_user()

		# Get the current admins.
		# How do I query the admin IDs into an array?
		# Take fetch results and loop through it, adding the ID to the admin list array.

		admins_query = Admin.query().order(Admin.name)
		adminList = []
		admins = admins_query.fetch(10)

		for admin in admins:
			adminList.append(admin.admin_id)

		## If user is logged in..
		if user:

			# Compare the user's ID to the admin list.
			if user.user_id in adminList or users.is_current_user_admin():

			# Generates a logout link. Put this in the navbar.
				url = users.create_logout_url(self.request.uri)
				url_linktext = 'Logout'

				template_values = {
				'admins': admins,
				'url': url,
				'url_linktext': url_linktext,
				}

				template = JINJA_EN.get_template('templates/admin.html')
				self.response.write(template.render({}))

			else:
				self.redirect('/error')

		## Force user to login.
		else:
			self.redirect(users.create_login_url(self.request.uri))

	def post(self):

		# Add new admin.
		admin_name = self.request.get('name')
		admin_id = self.request.get('admin_id')
		admin = Admin()

		admin.put()

# players Links
class bellerin(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/player/bellerin.html')
		self.response.write(template.render({}))
class buffon(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/player/buffon.html')
		self.response.write(template.render({}))
class gerrard(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/player/gerrard.html')
		self.response.write(template.render({}))
class lewan(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/player/lewan.html')
		self.response.write(template.render({}))
class neuer(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/player/neuer.html')
		self.response.write(template.render({}))
class ozil(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/player/ozil.html')
		self.response.write(template.render({}))
class pogba(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/player/pogba.html')
		self.response.write(template.render({}))

class ErrorPage(webapp2.RequestHandler):
	def get(self):
		template = JINJA_EN.get_template('templates/error.html')
		self.response.write(template.render({}))

app = webapp2.WSGIApplication([
	('/',MainPage),
	('/Login',login),
	('/Register',register),
	('/Players',playerList),
	('/PrivacyAndPolicy',privacyPolicy),
	('/admin',adminPage),
	('/error', ErrorPage),
	('/player_bellerin',bellerin),
	('/player_buffon',buffon),
	('/player_gerrard',gerrard),
	('/player_lewan',lewan),
	('/player_neuer',neuer),
	('/player_ozil',ozil),
	('/player_pogba',pogba),
	], debug=True)
