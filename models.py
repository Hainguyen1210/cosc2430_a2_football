from google.appengine.ext import ndb

class League(ndb.Model):
	name = ndb.StringProperty()

class Club(ndb.Model):
	league = ndb.KeyProperty(kind = League)
	name = ndb.StringProperty()

class Player(ndb.Model):
	displayName = ndb.StringProperty()
	fullName = ndb.StringProperty()
	dob = ndb.DateProperty()
	pob = ndb.StringProperty()

	price = ndb.FloatProperty()
	stat = ndb.IntegerProperty(repeated = True)
	height = ndb.FloatProperty()
	dominantFoot = ndb.StringProperty()

	club = ndb.KeyProperty(kind = Club)
	position = ndb.StringProperty()
	image = ndb.BlobProperty()
	video = ndb.StringProperty()
