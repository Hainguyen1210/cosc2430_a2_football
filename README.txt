GROUP: Yrevals Reyalp

Le Viet Hoang Dung - s3568452
Nguyen Ngoc Hai - s3577550


*****************************************
REFERENCES:
	Bootstrap Framework: getbootstrap.com
	Player info: futhead.com, transfermarkt.co.uk
	Bottom navigation bar design: gridgum.com
	Templates: w3schools.com

WORK DISTRIBUTION
*****************************************
Together:
	Design, find images, Layout and CSS.

Le Viet Hoang Dung - s3568452
	First layout design, Bottom bar, Index, Privacy&Policy, Player List, Player's detail.

Nguyen Ngoc Hai - s3577550
	Navigation bar, Login, Register,Find&Include videos, Decorate, Wrapup, Upload, Validate.  

*****************************************
NOTES
*****************************************
- We share code online and help each other on different parts of this assignment
- We split up the works evenly (mockup, coding)
- Took about 4 hours to figure out to upload to google app engine.
